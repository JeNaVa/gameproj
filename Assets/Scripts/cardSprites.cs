using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public class cardSprites : MonoBehaviour
{
    [SerializeField] private List<Sprite> _sprites;
    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        changeSprite();
    }

    public void changeSprite()
    {
        _spriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();

        int _randTexture;
        _randTexture = (int)Random.Range(0f, _sprites.Count - 1);
        _spriteRenderer.sprite = _sprites[_randTexture];
    }
    
    
}
