using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setNickName : MonoBehaviour
{
    private getNickname _nickname = new getNickname();
    private string name;
    
    void Start()
    {
        name = _nickname.GetName();
        this.GetComponent<Text>().text = name;
    }


}
