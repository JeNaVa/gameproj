using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getNickname : MonoBehaviour
{
    [SerializeField] private InputField _inputField;
    private static string name;

    public void SetName()
    {
        name = _inputField.GetComponent<InputField>().text;
    }
    
    public string GetName()
    {
        return name;
    }
 
}
