using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Compression;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using Unity.VisualScripting.Antlr3.Runtime.Tree;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    //Переменная берущая спрайт карты
    [SerializeField] private GameObject _teamCard;
    [SerializeField] private GameObject _teamRubashka;
    [SerializeField] private GameObject _enemyRubashka;
    [SerializeField] private GameObject _enemyCard;
    [SerializeField] private GameObject _cardsSpawnPointEnemy;
    [SerializeField] private GameObject _cardsSpawnPointTeam;
    
    [SerializeField] private Transform _mainCardSpawnPosEnemy;
    [SerializeField] private Transform _mainCardSpawnPosTeam;
    
    [SerializeField] private Text _timer;
    private Text _buttonText;

    private List<GameObject> _teamDeckOfCards = new List<GameObject>();
    private List<GameObject> _enemyDeckOfCards = new List<GameObject>();
    private List<GameObject> _teamDeckRubashka = new List<GameObject>();
    private List<GameObject> _enemyDeckRubashka = new List<GameObject>();
    private List<Transform> _teamCardsPosition = new List<Transform>();
    private List<Transform> _enemyCardsPosition = new List<Transform>();
    private List<Button> _buttons = new List<Button>();
    private static List<string> _teamLetters = new List<string>();
    private static List<string> _enemyLetters = new List<string>();
    private float timerStart = 5f;
    private Vector3 spawnPoint1PosEnemy;
    private Vector3 spawnPoint2PosTeam;
    private int pickUpCardIndex;
    private void Awake()
    {
        SpawnCards();
        GetCards();
        GetButtons();
        _buttons[0].onClick.AddListener(delegate { CheckValid();});
        _buttons[1].onClick.AddListener(delegate { CheckValid();});
        _buttons[2].onClick.AddListener(delegate { CheckValid();});
    }
    
    private void Update()
    {
        if (timerStart >= 0)
        {
            timerStart -= Time.deltaTime;
            _timer.text = Mathf.Round(timerStart).ToString();
        }
        
        if (timerStart < 0)
        {
            _enemyDeckOfCards[pickUpCardIndex].transform.position = _cardsSpawnPointTeam.transform.position;
            _teamDeckOfCards[pickUpCardIndex].transform.position = _cardsSpawnPointEnemy.transform.position;
            timerStart = 15;
            UnlockMainCard();
        }
    }

    public void SwitchSides(bool whoWin)
    {
        Vector3 spawnPosInTeamDeck = _enemyDeckOfCards[pickUpCardIndex - 1].transform.position;
        Vector3 spawnPosInEnemyDeck = _teamDeckOfCards[pickUpCardIndex - 1].transform.position;
        if (whoWin)
        {
            _teamDeckOfCards.Add(_enemyDeckOfCards[pickUpCardIndex]);
            _enemyDeckOfCards[pickUpCardIndex].transform.position = spawnPosInTeamDeck += new Vector3(0f,0.2f, 5f);
            _teamDeckOfCards[pickUpCardIndex].transform.position = spawnPosInTeamDeck += new Vector3(0f,0.2f, 5f);
            //_enemyDeckOfCards.RemoveAt(pickUpCardIndex);
        }
        else
        {
            _enemyDeckOfCards.Add(_teamDeckOfCards[pickUpCardIndex]);
            _teamDeckOfCards[pickUpCardIndex].transform.position = spawnPosInEnemyDeck += new Vector3(0f,0.2f, 0.1f);
            _enemyDeckOfCards[pickUpCardIndex].transform.position = spawnPosInEnemyDeck += new Vector3(0f,0.2f, 0.1f);
            //_teamDeckOfCards.RemoveAt(pickUpCardIndex);
        }
        timerStart = 15f;
    }
    
    public void CheckValid()
    {
        bool isTeamWin;
        for (int i = 0; i < _buttons.Count; i++)
        {
            if (_buttonText.text == _enemyLetters[i])
            {
                isTeamWin = false;
                SwitchSides(isTeamWin);
            }
            else
            {
                isTeamWin = true;
                SwitchSides(isTeamWin);
            }
        }
        Clean();
    }
    
    public void UnlockMainCard()
    {
        pickUpCardIndex = _enemyDeckOfCards.Count;
        Debug.Log(pickUpCardIndex);
        if ((pickUpCardIndex <= _enemyDeckOfCards.Count || pickUpCardIndex <= _teamDeckOfCards.Count))
        {
            Debug.Log(true);
            Debug.Log(pickUpCardIndex);
            --pickUpCardIndex;
        }
        _enemyDeckOfCards[pickUpCardIndex].transform.position = _mainCardSpawnPosTeam.position;
        _teamDeckOfCards[pickUpCardIndex].transform.position = _mainCardSpawnPosEnemy.position;
        Destroy(_teamDeckRubashka[pickUpCardIndex].gameObject); 
        Destroy(_enemyDeckRubashka[pickUpCardIndex].gameObject);
        ButtonsLettersCheck(_enemyDeckOfCards[pickUpCardIndex], _teamDeckOfCards[pickUpCardIndex]);
        for (int i = 0; i < _buttons.Count; i++)
        {
            _buttonText = _buttons[i].GetComponentInChildren<Text>();
            _buttonText.text = _enemyLetters[i];
        }
    }
    
    public void SpawnCards()
    {
        spawnPoint1PosEnemy = _cardsSpawnPointTeam.transform.position;
        spawnPoint2PosTeam = _cardsSpawnPointEnemy.transform.position;
        //Спавн Первой колоды вражеская
        for (int i = 0; i < 6; i++)
        {
            
            Instantiate(_teamCard, spawnPoint2PosTeam, quaternion.identity);
            Instantiate(_teamRubashka, (spawnPoint2PosTeam += new Vector3(0,0,-0.3f)), quaternion.identity);
            spawnPoint2PosTeam += new Vector3(-0.2f,0.2f, -0.2f);
        }
        
        //Спавн Второй колоды союзная
        for (int i = 0; i < 6; i++)
        {
            Instantiate(_enemyCard, spawnPoint1PosEnemy, quaternion.identity);
            Instantiate(_enemyRubashka, (spawnPoint1PosEnemy += new Vector3(0,0,-0.3f)), quaternion.identity);
            spawnPoint1PosEnemy += new Vector3(0.2f,0.2f, -0.2f);
        }
        
    }

    public void GetCards()
    {
        foreach (var e in GameObject.FindGameObjectsWithTag("TeamCard"))
        {
            _teamDeckOfCards.Add(e);
        }
        
        foreach (var e in GameObject.FindGameObjectsWithTag("EnemyCard"))
        {
            _enemyDeckOfCards.Add(e);
        }
        
        foreach (var e in GameObject.FindGameObjectsWithTag("RubashkaEnemy"))
        {
            _enemyDeckRubashka.Add(e);
        }
        
        foreach (var e in GameObject.FindGameObjectsWithTag("RubashkaTeam"))
        {
            _teamDeckRubashka.Add(e);
        }

        _enemyDeckOfCards.RemoveAt(0);
        _teamDeckOfCards.RemoveAt(0);
        _enemyDeckRubashka.RemoveAt(0);
        _teamDeckRubashka.RemoveAt(0);
    }

    public void ButtonsLettersCheck(GameObject cardTeam, GameObject cardEnemy)
    {
        SpriteRenderer srTeam = cardTeam.GetComponent<SpriteRenderer>();
        SpriteRenderer srEnemy = cardEnemy.GetComponent<SpriteRenderer>();
        //Добавление букв в союзную карту
        if (srTeam.sprite.name == "EVF_Card")
        {
            _teamLetters.Add("Е");
            _teamLetters.Add("В");
            _teamLetters.Add("Ф");
        }
        else if (srTeam.sprite.name == "ISHYAJ_Card")
        {
            _teamLetters.Add("И");
            _teamLetters.Add("Щ");
            _teamLetters.Add("Ж");
        }
        else if (srTeam.sprite.name == "SHGYA_Card")
        {
            _teamLetters.Add("Ш");
            _teamLetters.Add("Г");
            _teamLetters.Add("Я");
        }
        else if (srTeam.sprite.name == "STO_Card")
        {
            _teamLetters.Add("С");
            _teamLetters.Add("Т");
            _teamLetters.Add("О");
        }
        else if (srTeam.sprite.name == "TXB_Card")
        {
            _teamLetters.Add("Ъ");
            _teamLetters.Add("Х");
            _teamLetters.Add("Б");
        }
        //Добавление букв во вражескую карту
        if (srEnemy.sprite.name == "EVF_Card")
        {
            _enemyLetters.Add("Е");
            _enemyLetters.Add("В");
            _enemyLetters.Add("Ф");
        }
        else if (srEnemy.sprite.name == "ISHYAJ_Card")
        {
            _enemyLetters.Add("И");
            _enemyLetters.Add("Щ");
            _enemyLetters.Add("Ж");
        }
        else if (srEnemy.sprite.name == "SHGYA_Card")
        {
            _enemyLetters.Add("Ш");
            _enemyLetters.Add("Г");
            _enemyLetters.Add("Я");
        }
        else if (srEnemy.sprite.name == "STO_Card")
        {
            _enemyLetters.Add("С");
            _enemyLetters.Add("Т");
            _enemyLetters.Add("О");
        }
        else if (srEnemy.sprite.name == "TXB_Card")
        {
            _enemyLetters.Add("Ъ");
            _enemyLetters.Add("Х");
            _enemyLetters.Add("Б");
        }
    }
    
    public void GetButtons()
    {
        foreach (var e in GameObject.FindObjectsOfType<searchButton>())
        {
            _buttons.Add(e.GetComponent<Button>());
        }
    }

    public void Clean()
    {
        _teamLetters.Clear();
        _enemyLetters.Clear();
        for (int i = 0; i < _buttons.Count; i++)
        {
            _buttonText = _buttons[i].GetComponentInChildren<Text>();
            _buttonText.text = "";
        }
        
    }

}
